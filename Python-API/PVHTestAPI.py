import requests #Library to download 
import json #Native of Python

username = "Administrator"
password = "centric8"

"""
Connection token
"""

url = "http://localhost/csi-requesthandler/api/v2/session" #URL for connection
payload = {"username": username, "password": password} #body to send payload 
payload = json.dumps(payload) #body transform into json

headerConnection = {
  'Content-Type': 'application/json',
  'Connection': 'Keep-Alive'
} #headers parameters  

response = requests.request("POST", url, headers=headerConnection, data=payload) #We are sending the request
connectionToken = str(response.cookies.get_dict()).replace('{', '').replace('}', '').replace('\'', '').replace(',', ';').replace("JSESSIONID:", "JSESSIONID=").replace("\"centric:", "SecurityTokenURL=\"centric:").replace(" ", '')
#We get cookies of the request and reformat it to integrate it to our next requests
print("You have a connectionToken !!!")


#GET few styles

headersRequest = {
  'Cookie': connectionToken,
  'Accept': '*/*',
  'Content-Type': 'application/json',
  'Connection': 'Keep-Alive'
} #Header for request

url2 = "http://localhost/csi-requesthandler/api/v2/styles/?limit=10" #URL send for our request

response2 = requests.request("GET", url2, headers=headersRequest)

result = (response2.json())
print("Below the style we get with our request :")
for value in result :

    print("id = "+value['id']+", style node name = "+value['node_name']+", cntcolorway = "+str(value['cnt_colorway'])) #We print the first style we got and his id



#Put for each style we get before an update on description and active


for value in result :

    url3 = "http://localhost/csi-requesthandler/api/v2/styles/"+value['id']
    payload = {"description": "Test PVH API !", "active": "false"} #body to send payload 
    payload = json.dumps(payload) #body transform into json
    response3 = requests.request("PUT", url3, headers=headersRequest, data=payload)
    result3 = response3.json()
    print("We have update : "+result3['id']+', '+result3['node_name'])


#POST method for create a style

urlGetSeason = 'http://localhost/csi-requesthandler/api/v2/seasons/?node_name=Test3'
seasonId = requests.request("GET", urlGetSeason, headers=headersRequest).json()[0]['id'] #Get season id we want
print(seasonId)
urlGetCat1 = 'http://localhost/csi-requesthandler/api/v2/category1s/?parent_season='+seasonId+'&node_name=Brand'
cat1Id = requests.request("GET", urlGetCat1, headers=headersRequest).json()[0]['id'] #Get category 1 id we want link to the season
print(cat1Id)
urlGetCat2 = 'http://localhost/csi-requesthandler/api/v2/category2s/?category_1='+cat1Id+'&code=789547'
cat2Id = requests.request("GET", urlGetCat2, headers=headersRequest).json()[0]['id'] #Get category 2 id we want link to the category 1
print(cat2Id)
urlGetCollec= 'http://localhost/csi-requesthandler/api/v2/collections/?category_2='+cat2Id+'&node_name=Collec'
collecId = requests.request("GET", urlGetCollec, headers=headersRequest).json()[0]['id'] #Get collection id we want link to the category 2
print(collecId)

urlGetStyleType='http://localhost/csi-requesthandler/api/v2/style_types/?node_name=Style'
styleTypeId = requests.request("GET", urlGetStyleType, headers=headersRequest).json()[0]['id'] #Get the style type we want

urlStyleCreation = 'http://localhost/csi-requesthandler/api/v2/collections/'+collecId+'/hierarchy' #URL for create style refer to collec id
payload = {
  'node_name': "StyleCreateForPVH",
  'active': 'true',
  'code': '0005894',
  'product_type': styleTypeId
} #Body of required attributes !
payload = json.dumps(payload)
response3 = requests.request("POST", urlStyleCreation, headers=headersRequest, data=payload) #Send the request
resultCreate = response3.json()
print("We have create a new style named "+resultCreate['node_name'])
