import requests #Library to download 
import json #Native of Python

username = str(input("Enter your username : "))
password = str(input("Enter your password : "))

"""
Connection token
"""
url = "http://localhost/csi-requesthandler/api/v2/session" #URL for connection
payload = {"username": username, "password": password} #body to send payload 
payload = json.dumps(payload) #body transform into json

headerConnection = {
  'Content-Type': 'application/json',
  'Connection': 'Keep-Alive'
} #headers parameters  

response = requests.request("POST", url, headers=headerConnection, data=payload) #We are sending the request
connectionToken = str(response.cookies.get_dict()).replace('{', '').replace('}', '').replace('\'', '').replace(',', ';').replace("JSESSIONID:", "JSESSIONID=").replace("\"centric:", "SecurityTokenURL=\"centric:").replace(" ", '')
#We get cookies of the request and reformat it to integrate it to our next request
if response.status_code == 200:
  print("You have a connectionToken !!!")
else :
  print("We can't connect to the PLM relaunch the script")
  exit()

choice = '0'

headersRequest = {
  'Cookie': connectionToken,
  'Accept': '*/*',
  'Content-Type': 'application/json',
  'Connection': 'Keep-Alive'
} #Header for request
while choice != '4' :
  print("Please choose a choice : ")
  print("1 = get few styles")
  print("2 = update those few styles")
  print("3 = create a new style !")
  print("4 = exit")
  choice = input("your choice : ")
  if choice == '1' or choice == '2':
    #GET few styles
    url2 = "http://localhost/csi-requesthandler/api/v2/styles/?limit=5" #URL send for our request

    response2 = requests.request("GET", url2, headers=headersRequest)

    result = (response2.json())
    print("Below the style we get with our request :")
    for value in result :
      print("********************************************************************************")
      print("Style : "+value['id']+", "+value['node_name']+". Attributes :")
      for key in value:
        print(key +" => "+str(value[key])) #We print the first style we got and his id
    print("*************************************************************************************")

  
  #Put for each style we get before an update on description and active

    if choice == '2':
      description = str(input("Please choose new description for this style : "))
      active = ''
      while active != 'true' and active != 'false':
          active= str(input('Style will be active or inactive (true for active, false for inactive) : '))
      for value in result :
          url3 = "http://localhost/csi-requesthandler/api/v2/styles/"+value['id']
          payload = {"description": description, "active": active} #body to send payload 
          payload = json.dumps(payload) #body transform into json
          response3 = requests.request("PUT", url3, headers=headersRequest, data=payload)
          result3 = response3.json()
          print("We have update : "+result3['id']+', '+result3['node_name'])
          print("*************************************************************************************")


  #POST method for create a style
  if choice =='3':
    name = str(input("Choose a name for your style : "))
    code = str(input("Choose a code for your style : "))
    urlGetSeason = 'http://localhost/csi-requesthandler/api/v2/seasons/?node_name=Test3'
    seasonId = requests.request("GET", urlGetSeason, headers=headersRequest).json()[0]['id'] #Get season id we want
    urlGetCat1 = 'http://localhost/csi-requesthandler/api/v2/category1s/?parent_season='+seasonId+'&node_name=Brand'
    cat1Id = requests.request("GET", urlGetCat1, headers=headersRequest).json()[0]['id'] #Get category 1 id we want link to the season
    urlGetCat2 = 'http://localhost/csi-requesthandler/api/v2/category2s/?category_1='+cat1Id+'&code=789547'
    cat2Id = requests.request("GET", urlGetCat2, headers=headersRequest).json()[0]['id'] #Get category 2 id we want link to the category 1
    urlGetCollec= 'http://localhost/csi-requesthandler/api/v2/collections/?category_2='+cat2Id+'&node_name=Collec'
    collecId = requests.request("GET", urlGetCollec, headers=headersRequest).json()[0]['id'] #Get collection id we want link to the category 2

    urlGetStyleType='http://localhost/csi-requesthandler/api/v2/style_types/?node_name=Style'
    styleTypeId = requests.request("GET", urlGetStyleType, headers=headersRequest).json()[0]['id'] #Get the style type we want

    urlStyleCreation = 'http://localhost/csi-requesthandler/api/v2/collections/'+collecId+'/hierarchy' #URL for create style refer to collec id
    payload = {
      'node_name': name,
      'active': 'true',
      'code': code,
      'product_type': styleTypeId
    } #Body of required attributes !
    payload = json.dumps(payload)
    response3 = requests.request("POST", urlStyleCreation, headers=headersRequest, data=payload) #Send the request
    resultCreate = response3.json()
    print("We have create a new style named "+resultCreate['node_name'])
    print("*************************************************************************************")

print("Thanks to have use this little API !")